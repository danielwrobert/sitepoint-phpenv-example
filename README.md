## Example Guestbook App
 This is an example application for SitePoint's [Jump Start PHP Environment](http://www.amazon.com/Jump-Start-Environment-Bruno-Skvorc/dp/0994182643) book.

The application is developed in the last chapter of the book, building on the theory from previous chapters.